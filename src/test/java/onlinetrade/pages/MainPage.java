package onlinetrade.pages;

import com.codeborne.selenide.SelenideElement;
import onlinetrade.data.AbstractPage;

import static com.codeborne.selenide.Selenide.$x;

public class MainPage extends AbstractPage {
    public static SelenideElement lkButton = $x("//*[@title='Вход в Личный кабинет']");
    public static SelenideElement loginField = $x("//*[@name='login']");
    public static SelenideElement passwordField = $x("//*[@name='password']");
    public static SelenideElement submitButton = $x("//*[contains(@name,'submit')]");
    public static SelenideElement resumeOnWeb = $x("//*[contains(@title,'Вернуться на сайт')]");

    public MainPage(boolean isToOpen) {
        super(isToOpen);
    }

}