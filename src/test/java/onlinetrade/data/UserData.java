package onlinetrade.data;

import org.testng.annotations.DataProvider;

public class UserData {
    @DataProvider(name = "User")
    public Object[][] UserDataObject() {
        return new Object[][]{
                {"mkya-nn@mail.ru", "44BzR3C"}
        };
    }

    @DataProvider(name = "BadUser")
    public Object[][] BadUserDataObject() {
        return new Object[][]{
                {"nn@mail.ru", "44BzR3C"},
                {"44BzR3C", "44BzR3C"}
        };
    }
}
