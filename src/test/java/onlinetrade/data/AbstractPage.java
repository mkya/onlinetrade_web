package onlinetrade.data;

import onlinetrade.pages.MainPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.open;
import static onlinetrade.pages.MainPage.*;
import static onlinetrade.pages.MainPage.submitButton;

public abstract class AbstractPage {
    public static final Logger log = LoggerFactory.getLogger(AbstractPage.class.getName());
    public static String link = "https://www.onlinetrade.ru/";

    public AbstractPage(boolean openNewPage) {
        if (openNewPage) {
        open(link);
        log.info("Open new page: " + link);
        }
    }

    public static MainPage loginUser(String login, String password) {
        lkButton.click();
        loginField.setValue(login);
        passwordField.setValue(password);
        submitButton.click();
        return new MainPage(false);
    }


}