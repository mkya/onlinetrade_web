package onlinetrade.tests;

import io.qameta.allure.Epic;
import onlinetrade.data.UserData;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

@Epic(value = "Авторизация")
@Test(groups = "login")
public class BaseTest extends AbstractTest {

    @Test(dataProvider = "User", dataProviderClass = UserData.class)
    public void loginPass(String login, String password) {
        AbstractTest.openMainPage()
                .loginUser(login, password);
        Assert.assertTrue($(".huab__cell__text.orange").getText().equals("MK"));
        closeWebDriver();
    }

    @Test(dataProvider = "BadUser", dataProviderClass = UserData.class)
    public void loginFail(String login, String password) {
        AbstractTest.openMainPage()
                .loginUser(login, password);
        Assert.assertTrue($x("//*[text()='Указан неверный e-mail или пароль']").exists());
    }
}
