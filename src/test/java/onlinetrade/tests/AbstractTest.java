package onlinetrade.tests;

import onlinetrade.pages.MainPage;

import static com.codeborne.selenide.Selenide.open;
import static onlinetrade.pages.MainPage.*;

public abstract class AbstractTest {
    public static MainPage openMainPage() {
        open(link);
        log.info("Open new page: " + link);
        if(resumeOnWeb.exists()) resumeOnWeb.click();
        return new MainPage(false);
    }

}
