# onlinetrade_web



## Getting started


1. Импортируем проект по ссылке из гит репозитория.
2. Импортируем зависимости maven
3. Нужно создать TestNG конфигурацию и указать **полный** путь до xml suite файла - src/test/java/onlinetrade/xml/allsuites.xml
4. В случае, если chrome драйвер не нашёлся, попробуйте в метод AbstractPage добавить строку
"System.setProperty("webdriver.chrome.driver", "C:\\Users\\mkya-\\IdeaProjects\\onlinetrade_web\\chromedriver.exe");"
Указав свой путь до файла chromedriver.exe (приложил драйвер в корень проекта).
